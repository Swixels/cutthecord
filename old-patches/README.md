## CutTheCord: Old Patches

This folder contains old patches.

Here's why these patches are no longer maintained in this form:
- nohiddenchannels: Moved into slashcommands as a slash command
- notyping: Moved into slashcommands as a slash command
- pureevil: It's now upstream (yay.)
- compact: It's just a pain to maintain.
- smalltime: It's just a pain to maintain.
- customdefaultemoji: There doesn't seem to be default emojis anymore?
