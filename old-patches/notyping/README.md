## DisTok CutTheCord: No Typing Patch

This patch disables the typing event.

#### Available and tested on:
- 8.3.6g
- 8.3.9g
- 8.4.1g
- 8.4.2g
- 8.4.3g
- 8.4.4g
- 8.4.5g
- 8.4.8
- 8.5.0
- 8.5.1
- 8.5.3
- 8.5.4
- 8.5.5
- 8.5.6
- 8.7.6
- 8.8.4
- 8.8.8
- 8.9.6
- 8.9.7
- 8.9.8
- 8.9.9
- 9.0.0
- 9.0.1
- 9.0.2
- 9.0.3

