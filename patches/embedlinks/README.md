## DisTok CutTheCord: Embed Links

Bring back embed links when it's the only thing contained in the message.

![](https://elixi.re/i/xcn7vul5.png)

to

![](https://elixi.re/i/xdkl2p9x.png)

#### Available and tested on:
- 36.5
- 38.0
- 38.1
- 40.04
- 41.02
- 41.05
- 41.06
- 41.10
- 41.11
- 42.0
- 42.1
- 42.3
- 44-alpha2
- 44-alpha4
- 44.5
- 44.6
- 45.2
- 46.0
- 46.3
- 48.0
- 48.1
- 48.2
- 49.1
- 49.2
- 49.8
- 49.10
- 49.12
- 49.13

